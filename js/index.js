const live = document.querySelector("#live");
const run = document.querySelector("#run");
const copy = document.querySelector("#copy");
const result = document.querySelector(".result-code");
const source = document.querySelector(".source-code");
let gutterCounter = 1;
let currentCode;

/**
 * Запуск редактора, создание первой строки для ввода...
 */
createLine(source);

live.addEventListener("click", () => {
    live.classList.toggle("active");
    if(live.classList.contains("active")){
        lineBreaking("Live mode is under development");
    }
    else{
        result.replaceChildren();
        gutterCounter = 1;
    }
});

/**
 * Функция для того чтобы задать строку для расшифровки если в данный момент фокус находится на другом элементе.
 * @returns Строку для расшифровки (последняя не пустая строка в блоке)
 */
function getCurrentCode() {
    const [...allCodeLines] = document.querySelectorAll(".source-code .code");
    const dataCodeLines = allCodeLines.map((line) => {
        if (line.textContent !== "") {
            return line;
        }
    });
    return dataCodeLines.at(-1).textContent;
}

run.addEventListener("mouseover", () => {
    // если фокус находится на строке с кодом то передаем ее значение
    if(document.activeElement.localName === "code"){
        currentCode = document.activeElement.textContent;
    }
    else{ // а иначе вызываем getCurrentCode()
        currentElement = getCurrentCode();
    }
});
run.addEventListener("click", () => {
    if(currentCode !== ""){
        parseCode(currentCode);
    }    
});

/**
 * Обработка нажатия кнопки Copy. Если текст выделен то копируется выделенный фрагмент,
 * если выделения нету то копируется весь HTML код.
 */
copy.addEventListener("click", () => {
    if (document.getSelection().type === "Range") {
        document.getSelection();
        document.execCommand("copy");
        document.getSelection().empty(); // Снять выделение
    } else {
        const range = new Range();
        range.selectNodeContents(result);
        document.getSelection().addRange(range);
        document.execCommand("copy");
        document.getSelection().removeAllRanges(); // Снять выделение
    }
});

/**
 * Функция обрабатывает нажатие клавиш клавиатуры.
 * @param {object} e Event, передаем всё событие, чтобы использовать разные параметры.
 */
function keyPressHandler(e) {
    const codeLine = e.target;
    if (e.key === "Enter") {
        e.preventDefault();
        createLine(source, codeLine);
        if (codeLine.textContent !== "") {
            parseCode(codeLine.textContent);
        }
    } else if (
        e.key === "Backspace" &&
        codeLine.textContent === "" &&
        codeLine.previousElementSibling.innerText > 1
    ) {
        setCursor(codeLine.parentElement.previousElementSibling.children[1]);
        codeLine.parentElement.remove();
        gutterCount(".source-code .gutter");
    } else if (e.key === "ArrowUp") {
        try {
            setCursor(
                codeLine.parentElement.previousElementSibling.children[1]
            );
        } catch (e) {}
    } else if (e.key === "ArrowDown") {
        try {
            setCursor(codeLine.parentElement.nextElementSibling.children[1]);
        } catch (e) {}
    } else {
        // console.log(e.key);
    }
}

/**
 * Функция создает рядок с порядковым номером и данными, если они передаются.
 * @param {object} parent Целевое окно в которое нужно добавить рядок.
 * @param {string} data Строка которая будет находится в рядке. По умолчанию пустая строка.
 */
function createLine(parent, target, data = "") {
    const line = document.createElement("div");
    line.className = "line";

    const gutter = document.createElement("span");
    gutter.className = "gutter";
    if (parent === source) {
        if (!target) {
            gutter.innerText = 1;
        }
        if (target) {
            gutter.innerText = ++target.previousElementSibling.textContent;
        }
    } else if (parent === result) {
        gutter.innerText = gutterCounter++;
    }

    const code = document.createElement("code");
    code.className = "code";
    if (parent === source) {
        code.setAttribute("contenteditable", true);
    }
    if (parent === result) {
        code.textContent = data;
    }
    code.setAttribute("spellcheck", false);

    code.addEventListener("keydown", (e) => {
        keyPressHandler(e);
    });

    line.append(gutter, code);
    if (!target) {
        parent.append(line);
    } else if (target) {
        target.parentElement.after(line);
        gutterCount(".source-code .gutter");
    }

    code.focus();
}

/**
 * Функция пробегается по всем рядкам и перенумеровывает их.
 * @param {string} target Передаем CSS селектор элементов которые нужно пронумеровать.
 */
function gutterCount(target) {
    const gutter = document.querySelectorAll(target);
    gutter.forEach((el, index) => {
        el.innerText = ++index;
    });
}

/**
 * Функция устанавливает фокус на переданный Node и ставит курсор в конец строки.
 * @param {object} target Node
 */
function setCursor(target) {
    target.focus();
    const range = new Range();
    range.selectNodeContents(target);
    range.collapse(false);
    document.getSelection().empty();
    document.getSelection().addRange(range);
}

/**
 * Класс Element принимает в конструктор экземпляра объект со всеми нужными параметрами и создает HTML элемент с заданым именем.
 * Также в конструкторе сразу при создании экземпляра вызывается метод build() который добавляет в HTML элемент все заданые свойства и атрибуты.
 * Метод render() строит из элемента красивое HTML дерево со всеми отступами и переносами и возвращает строку с необходимой разметкой.
 */
class Element {
    constructor(param = {}) {
        this.element = document.createElement(param.name || "div");
        this.id = param.id;
        this.class = param.class;
        this.attr = param.attr || [];
        this.children = param.children || [];
        this.content = param.content;
        this.element.level = param.level || 0;
        this.build();
    }

    build() {
        if (this.id) {
            this.element.id = this.id;
        }
        if (this.class) {
            this.element.className = this.class;
        }
        if (this.attr) {
            if (Array.isArray(this.attr)) {
                this.attr.forEach((attr) => {
                    if (typeof attr === "string") {
                        let attribute = attr.split("=");
                        this.element.setAttribute(attribute[0], attribute[1] ?? "");
                    }
                });
            }
        }
        this.element.content = this.content;
        if (this.children) {
            if (Array.isArray(this.children)) {
                this.children.forEach((child) => {
                    const childElement = new Element(child);
                    this.element.append(childElement.element);
                });
            }
        }
    }

    render() {
        const tab = "    ";
        const tag = /<[^<>]+>/g;
        let output = "";
        function dropElement(element) {
            const [...childrens] = element.children;
            childrens.forEach((child) => {
                const openTag = child.outerHTML.match(tag)[0];
                const closeTag = child.outerHTML.match(tag).at(-1);
                let content;
                if (child.content) {
                    content = child.content;
                }
                if (
                    child.children.length === 0 &&
                    content &&
                    content.length < 25
                ) {
                    output += `${tab.repeat(
                        child.level - 1
                    )}${openTag}${content}${closeTag}\n`;
                } else if (child.children.length > 0 && !content) {
                    output += `${tab.repeat(child.level - 1)}${openTag}\n`;
                    dropElement(child);
                    output += `${tab.repeat(child.level - 1)}${closeTag}\n`;
                } else if (child.children.length > 0 && content) {
                    output += `${tab.repeat(child.level - 1)}${openTag}\n`;
                    output += `${tab.repeat(child.level)}${content}\n`;
                    dropElement(child);
                    output += `${tab.repeat(child.level - 1)}${closeTag}\n`;
                } else if (
                    child.children.length === 0 &&
                    content &&
                    content.length > 25
                ) {
                    output += `${tab.repeat(child.level - 1)}${openTag}\n`;
                    output += `${tab.repeat(child.level)}${content}\n`;
                    output += `${tab.repeat(child.level - 1)}${closeTag}\n`;
                } else if (child.children.length === 0 && !content) {
                    output += `${tab.repeat(
                        child.level - 1
                    )}${openTag}${closeTag}\n`;
                }
            });
        }
        dropElement(this.element);
        return output;
    }
}

/**
 * Функция разбивает строку на рядки по переносам и для каждого рядка вызывает функцию создания рядка.
 * @param {string} string Строка которую нужно разбить по рядкам.
 */
function lineBreaking(string) {
    if (string && typeof string === "string") {
        string.split("\n").forEach((string) => {
            createLine(result, "", string);
        });
    } else {
        console.log("Incorrect data in lineBreaking()");
        return;
    }
}

/**
 * Функция принимает строку с сокращенным кодом и разбирает ее на элементы,
 * задавая им необходимые параметры и уровни вложенности.
 * Создает корневой объект wrapper, в котором формируется все дерево.
 * Из wrappera создается экземпляр класса Element и вызывается метод render(),
 * который передается в функцию разбиения на рядки для дальнейшего вывода.
 * @param {string} string Строка содержащая сокращенный код.
 */
function parseCode(string) {
    if (!string || typeof string !== "string") {
        console.log(
            "The function 'parseCode' received incorrect data, the data must have type 'string'"
        );
    } else if (string) {
        const regUp = /\^+/g;
        const wrapper = {};
        let currentElement = wrapper;
        currentElement.level = 0;

        // разбиваем по уровням
        const levels = string.split(">");
        levels.forEach((level) => {
            currentElement.children = [];

            // каждый уровень проверяем на соседей
            if (!level.includes("+")) {
                // если соседей нет то проверяем нет ли подъема вверх
                if (!level.includes("^")) {
                    // если нет ни соседей ни подъемов то создаем элемент и переходим к дочерним
                    const element = parseElement(level, currentElement.level);
                    currentElement.children.push(element);
                    currentElement = element;
                } else if (level.includes("^")) {
                    // если есть подъем вверх
                    // разделяем на дочерний и тот что нужно поднять
                    const index = regUp.exec(level).index; // индекс разделителя
                    const dividerQuantity = level.match(regUp)[0].length; // кол-во разделителей (на сколько уровней нужно поднять)
                    const childPart = level.substring(0, index); // дочерний элемент
                    const upPart = level.substring(index + dividerQuantity); // элемент который нужно поднять
                    const childElement = parseElement(
                        childPart,
                        currentElement.level
                    );
                    currentElement.children.push(childElement); // закидываем дочерний в текущий элемент
                    const searchLevel = currentElement.level - dividerQuantity;
                    currentElement = findParentObject(wrapper, searchLevel); // устанавливаем текущим элементом тот в который нужно поднять элемент
                    const upElement = parseElement(
                        upPart,
                        currentElement.level
                    );
                    currentElement.children.push(upElement);
                    currentElement = upElement;
                }
            } else if (level.includes("+")) {
                // если соседи есть то разбиваем по соседям
                const elements = level.split("+");
                elements.forEach((el, i, arr) => {
                    // если сосед не содержит подъемов вверх
                    if (!el.includes("^")) {
                        // создаем элемент и проверяем не последний ли он
                        const element = parseElement(el, currentElement.level);
                        currentElement.children.push(element);
                        if (i === arr.length - 1) {
                            // если это был последний элемент то назначаем его текущим
                            currentElement = element;
                        }
                    }
                    // если есть подъем вверх
                    else if (el.includes("^")) {
                        const index = regUp.exec(el).index;
                        const dividerQuantity = el.match(regUp)[0].length;
                        const childPart = el.substring(0, index);
                        const upPart = el.substring(index + dividerQuantity);
                        const childElement = parseElement(
                            childPart,
                            currentElement.level
                        );
                        currentElement.children.push(childElement);
                        const searchLevel =
                            currentElement.level - dividerQuantity;
                        currentElement = findParentObject(wrapper, searchLevel);
                        const upElement = parseElement(
                            upPart,
                            currentElement.level
                        );
                        currentElement.children.push(upElement);
                        if (i === arr.length - 1) {
                            currentElement = upElement;
                        }
                    }
                });
            }
        });

        lineBreaking(new Element(wrapper).render());
    }
}

/**
 * Функция для поиска нужного родителя для подьема элементов.
 * @param {object} object Корневой объект в который все упаковывается.
 * @param {number} searchLevel Искомый уровень вложенности.
 * @returns Возвращает объект-родитель нужного уровня.
 */
function findParentObject(object, searchLevel) {
    let level;
    let target;

    if(searchLevel >= 0){
        level = searchLevel;
    }
    else{
        lineBreaking("You made a mistake in raising the levels, check your code, level is set to the lowest possible");
        level = 0;
    }
    
    if (object.level === level) {
        target = object;
    } else {
        if (object.children) {
            object.children.forEach((child) => {
                target = findParentObject(child, level);
            });
        }
    }
    return target;
}

/**
 * Функция преобразует строчное сокращенное представление элемента в объект с данными для его создания.
 * @param {string} elementStr Строка с данными отдельного элемента.
 * @returns {object} Возвращает объект с данными для создания элемента.
 */
function parseElement(elementStr, level) {
    if (!elementStr || typeof elementStr !== "string") {
        console.log(
            "The function 'parseElement' received incorrect data, the data must have type 'string'"
        );
    } else if (elementStr) {
        const namePattern = /^[a-z].*?(?=\.|#|\[|{|$)/;
        const classPattern = /[.].*?(?=#|\[|{|$)/;
        const idPattern = /[#].*?(?=\.|\[|{|$)/;
        const attrPattern = /[\[].*[\]]/;
        const contentPattern = /[{].*[}]/;

        const element = {};
        if (elementStr.match(namePattern)) {
            element.name = elementStr.match(namePattern).toString();
        }
        if (elementStr.match(classPattern)) {
            element.class = elementStr
                .match(classPattern)
                .toString()
                .replaceAll(".", " ")
                .trim();
        }
        if (elementStr.match(idPattern)) {
            element.id = elementStr.match(idPattern).toString().slice(1);
        }
        if (elementStr.match(attrPattern)) {
            element.attr = elementStr
                .match(attrPattern)
                .toString()
                .slice(1, -1)
                .split(" ");
        }
        if (elementStr.match(contentPattern)) {
            element.content = elementStr
                .match(contentPattern)
                .toString()
                .slice(1, -1);
        }
        element.level = ++level;
        return element;
    }
}

// !!!!!! Осталось реализовать группировку и умножение, также можно добавить распространенные сокращения тегов и автозаполнение имени тега в зависимости от контекста(например чтобы в ul без названия тега вкладывался li а не div).
// !!!!!! Реализовать режим реального времени.
// !!!!!! Сделать очистку полей

/*
const elPattern = /[\w.#^].*?([>+]|[^\^](?=\^)|$)/gi;
[] - attributes
{} - content
. - class
# - id

+ - sibling
> - child
^ - level up
() - group

* - multiplication
$, $@, $@- - numbering

!, : - HTML Emmet
*/